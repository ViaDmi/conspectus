# Конспект лекций по курсу COURSE
TeX-конспект лекций курса по COURSE
на основе класса [`conspectus`](https://gitlab.com/ViaDmi/conspectus). 
Гарантируется работа на версии класса 1.2.0.
По лекциям LECTURER.
SEMESTERS.

- [Конспект лекций по курсу COURSE](#конспект-лекций-по-курсу-course)
  - [Структура проекта](#структура-проекта)
    - [Описание директорий:](#описание-директорий)
    - [Команды `Makefile`:](#команды-makefile)

## Структура проекта
Все пути в проекте пишутся относительно корневой директории проекта.
Менять структуру и названия директорий, если Вы четко не понимаете, что делаете, строго **не** рекомендуется.
Ознакомьтесь с предлагаемой структурой организации проекта конспекта при классе `conspectus`.

### Описание директорий:
- `main`: директория с одноименным основным файлом, соединяющим разделы конспекта в один цельный документ. Также содержит стилевой файл, определяющий вид документа.
- `sectionNN`: директория с одноименным TeX'овским файлом соответствующей секции (раздела). Все одного класса: `\documentclass[../main/main.tex]{subfiles}`. **Это требование обязательно.**
    - `images`: поддиректория с изображениями, содержащая в себе все изображения (`pdf`-файлы с TeX'овскими `PGF/TikZ` исходниками (класс `standalone`), либо обычные `jpeg`/`png` изображения).

### Команды `Makefile`:
| Команда       | Действие |
| ------------- | -------- |
| `make build`  | собирает конспект целиком, оптимизирует итоговый `pdf` файл |
| `make images` | создает все изображения из TeX'овских исходников, которые находятся в директориях `./*/images` |
| `make view`   | открывает сам конспект `FILENAME.pdf` |
| `make clean`  | удаляет все рабочие вспомогательные файлы (`*.log`, `*.out` etc), а также все `pdf` файлы, за исключением самого конспекта `FILENAME.pdf` |

По умолчанию `make images` использует все доступные потоки процессора.

Запуск `make build` с переменной `GS=no`
```console
$ make build GS=no
```
после сборки конспекта **не** оптимизирует итоговый `pdf` файл утилитой [`ghostscript`](https://www.ghostscript.com/).
