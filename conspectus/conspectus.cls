% LaTeX class for making lecture notes
% Author: ViaDmi
% Version: 1.2.0
% The most recent version can be found here:
% https://gitlab.com/ViaDmi/conspectus
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{conspectus}[2022/04/30 conspectus LaTeX class]
\RequirePackage{ifthen}
\RequirePackage{etoolbox}

% disclaimer printing
\newboolean{disclaimer}
\setboolean{disclaimer}{true}
% for multifiles
\newboolean{multifiles}
\setboolean{multifiles}{false}

\DeclareOption{nodis}{\setboolean{disclaimer}{false}}
\DeclareOption{multifiles}{\setboolean{multifiles}{true}}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{extarticle}}
\ProcessOptions\relax
\LoadClass{extarticle}

%% basic
\RequirePackage{cmap}
\RequirePackage[T2A]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage[russian]{babel}
\RequirePackage[left=1.5cm,right=1.5cm,top=3cm,bottom=2cm]{geometry}

%% for modular projects
\ifthenelse{\boolean{multifiles}}%
    {\RequirePackage{subfiles}}{}%

%% math packages
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{amsfonts}
\RequirePackage[makeroom]{cancel}

%% special packages
\RequirePackage{hyperref}
\RequirePackage{caption}
\RequirePackage{enumitem}
\RequirePackage{multicol}
\RequirePackage{array}
\RequirePackage{xcolor}
\RequirePackage{calc}

%% packages for images
\RequirePackage{tikz}
\RequirePackage{subfig}
\RequirePackage{wrapfig}
\RequirePackage{graphicx}

%% standard style settings
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\renewcommand{\sectionmark}[1]{\markright{#1}}
\fancyhead[L]{\rightmark}
\fancyhead[R]{}
\setlength{\headheight}{\heightof{\Large \^A}} % fix headheight for other font's sizes

%% setting hyphenation
\lefthyphenmin=20
\righthyphenmin=20

%% macro for optional disclaimer
\def\optionalpreface#1{\gdef\@optionalpreface{#1}}
\def\@optionalpreface{}%

%% def macros for title page
\def\lecturer#1{\gdef\@lecturer{#1}}
\def\@lecturer{\@latex@warning@no@line{No \noexpand\lecturer given}}

\def\urladdress#1{\gdef\@urladdress{#1}}
\def\@urladdress{}

\newcommand{\@conspectuslogo}%
{%
    \includegraphics{conspectus/logo.pdf}
}

%% define title page using standard command maketitle     
\def\@maketitle{%
    \thispagestyle{empty}%
    \begin{center}
        \noindent\rule{\linewidth}{0.2pt}
        \noindent{\scshape\LARGE\ignorespaces Конспект лекций\\}%
        \noindent\rule{\linewidth}{0.2pt}%
        \vspace{0.05\paperheight}%
        
        \@conspectuslogo

        \vspace{0.05\paperheight}%
        {\Huge\bfseries\ignorespaces \@title \\}%
        
        \vspace{0.06\paperheight}
        {\Large\ignorespaces По лекциям \@lecturer \par}%
        \vspace{0.05\paperheight}%
        \vfill%
        \noindent{\Large\ignorespaces \@author \par}%
        \vspace{0.03\paperheight}%
        \noindent{\ignorespaces \@date}%
    \end{center}%
    \newpage%
}

\newcommand{\@disclaimer@box}{%
    \ifthenelse{\boolean{disclaimer}}{
        \begin{center}%
            \noindent{\color{red!75!black}\fbox{\textbf{Черновая версия: возможно большое количество различного рода ошибок!}}}
        \end{center}}{}%
}

\newcommand{\@print@url@sentence}{%
    \ifdefempty{\@urladdress}
        {}%
        {Исходники доступны по~адресу \mbox{\url{\@urladdress}}, чтобы конспект можно было редактировать
         и~дополнять при~необходимости, а~также исправлять найденные ошибки и~недостатки.}%
}

\newcommand{\@preface}
{%
    \subsection*{Предисловие}%
    \noindent Данный конспект является не~более чем цифровой версией обычной тетрадки с~записями материалов лекции.
    Прочтение данного конспекта не~заменит полноценное изучение курса. \@print@url@sentence
}

\newcommand{\@beginning@document}
{%
    \@maketitle%
    \@disclaimer@box
    \@preface%
    
    \@optionalpreface%
    \tableofcontents\newpage%
}

\sloppy

\AtBeginDocument
{%
    \hypersetup{%
        unicode=true,%
        pdftitle={Конспект \@title},%
        pdfsubject={\@title},%
        pdfauthor={\@author},%
        pdfkeywords={Конспект} {\@title},%
    }%
    % document
    \ifthenelse{\boolean{multifiles}}{%
        \ifthenelse{\equal{\jobname}{\detokenize{main}}}%
            {\@beginning@document}%
            {}%
        }
        {\@beginning@document}%
}

\AtEndDocument
{%
}
