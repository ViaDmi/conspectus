NAME := conspectus
GIT_VERSION := ${shell git describe --tags HEAD 2>/dev/null || echo unknown-version}
TEX_COMPILER := pdflatex -interaction=nonstopmode
FULL_PATH := $(if $(TEXMFHOME),${TEXMFHOME}/tex/latex,${HOME}/texmf/tex/latex)
SUCCESS_MARK := \033[1;32m[\342\234\224]\033[0m


install: install_class install_mkcons


install_class:
	printf "Installing class \033[1m$(NAME)\033[0m \033[;36m$(GIT_VERSION)\033[0m...\n"
	printf "Compilation \033[1m$(NAME)\033[0m logo...\n"
	make conspectus/logo.pdf>/dev/null
	printf "$(SUCCESS_MARK) \033[1m$(NAME)\033[0m logo is compiled.\n"
	printf "Installing in \033[2m$(FULL_PATH)\033[0m...\n"
	mkdir --parents $(FULL_PATH)/$(NAME)
	cp $(NAME)/$(NAME).cls $(FULL_PATH)/$(NAME).cls
	cp $(NAME)/logo.pdf $(FULL_PATH)/$(NAME)/logo.pdf
	printf "$(SUCCESS_MARK) Successfully installed in \033[2m$(FULL_PATH)\033[0m.\n"

conspectus/logo.pdf: conspectus/logo.tex
	@$(TEX_COMPILER) -output-directory $(dir $<) $<>$(dir $<)/logo.mk.log

uninstall_class:
	rm --recursive --force $(FULL_PATH)/$(NAME).cls $(FULL_PATH)/$(NAME) && \
		printf "$(SUCCESS_MARK) \033[1m$(NAME)\033[0m successfully uninstalled.\n"


MKCONS_BIN_PATH := ${HOME}/.local/bin
MKCONS_DAT_PATH := $(if $(XDG_DATA_HOME),${XDG_DATA_HOME}/mkcons,${HOME}/.local/share/mkcons)
install_mkcons:
	printf "Installing \033[1mmkcons\033[0m script...\n"
	mkdir --parents $(MKCONS_BIN_PATH) $(MKCONS_DAT_PATH)
	cp --recursive mkcons/mkcons $(MKCONS_BIN_PATH)
	cp --recursive mkcons/mkcons-data/* mkcons/mkcons-data/.gitignore $(MKCONS_DAT_PATH)
	chmod u+x $(MKCONS_BIN_PATH)/mkcons
	printf "$(SUCCESS_MARK) Successfully installed in \033[2m$(MKCONS_BIN_PATH)\033[0m and \033[2m$(MKCONS_DAT_PATH)\033[0m.\n"


uninstall_mkcons:
	rm --recursive --force $(MKCONS_BIN_PATH)/mkcons $(MKCONS_DAT_PATH) && \
    	printf "$(SUCCESS_MARK) \033[1mmkcons\033[0m successfully uninstalled.\n"


.PHONY:
.SILENT:
